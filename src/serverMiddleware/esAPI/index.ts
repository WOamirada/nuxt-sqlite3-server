import { eRequestTypes, IIncomingRequest, TResponseObject } from '@/EsBridgeAPI/types';
import { signin, verifyToken } from '@/EsBridgeAPI/mysqlConnection';

// create the connection to database

export default async function (req:IIncomingRequest, res:TResponseObject) {
  // req is the Node.js http request object
  console.log('REQUEST:');
  console.log(req.url);
  console.log('method: ' + req.method);

  // if (req.method in [eRequestTypes.post, eRequestTypes.put]) {}
  if (['/login', '/login/', '/signin', '/signin/'].includes(req.url.toLowerCase())) {
    res = await signin(req, res);
    res.end();
    return;
  }
  else {
    res = await verifyToken(req, res);
    if (!res) { return; }
  }
  // res is the Node.js http response object
  res.writeHead(200, { 'Content-Type': 'application/json' });
  res.write(
    JSON.stringify({
      message: 'call was successful !'
      // TODO: everytime new accestoken ?
    })
  );
  res.end();

  console.log('==============');
  // next is a function to call to invoke the next middleware
  // Don't forget to call next at the end if your middleware is not an endpoint!
}

/* import express from 'express'
const app = express()

app.use(express.urlencoded({
  extended: true
}));
app.use(express.json())

/!**
 * logic for our api will go here
 *!/
export default {
  path: '/API',
  handler: app
} */
