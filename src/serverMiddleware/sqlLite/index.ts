export default {
  handler(req:object, res:{write:(s:string)=>void,end:()=>void}) {
    console.log("serverMiddleware!")
    res.write("test complete")
    res.end()
  },
  path: '/test'
}
