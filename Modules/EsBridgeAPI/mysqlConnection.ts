import mysql, { Connection } from 'mysql2';
import { IIncomingRequest, TResponseObject } from '@/EsBridgeAPI/types';
import { parseReq } from './parser';

const jwt = require('jsonwebtoken');
// const bcrypt = require('bcrypt');

interface IRequest extends IIncomingRequest{
  body:{
    password:string
  }
}

interface IResponse extends TResponseObject{}

export const sql = mysql.createConnection({
  host: 'localhost',
  port: 3306,
  user: 'root',
  password: '123456',
  database: 'customerDB'
});

function handleSQLError (err) {
  console.log(err);
}

function query (query:string) {
  return new Promise((resolve, reject) => {
    sql.query(
      query,
      (err, result) => {
        if (err) {
          handleSQLError(err);
          reject(err);
        }
        else {
          resolve(result);
        }
      }
    );
  });
}

function verify (req, token) {
  return new Promise((resolve, reject) => {
    jwt.verify(
      token,
      apiKey,
      (err, decode) => {
        if (err) { reject(err); }
        else { resolve(decode); }
      }
    );
  });
}

let apiKey = '';
async function getApiKey () {
  const temp = await query('SELECT svalue FROM ini WHERE  fkey="apiKey"');
  // @ts-ignore
  apiKey = temp[0].svalue;
}
getApiKey();
const USERID = Math.floor(Math.random() * 10000);

export async function signin (req, res:TResponseObject) {
  const reqBody = await parseReq(req);
  console.log('body ?');
  console.log(reqBody);
  const body = reqBody[0];

  const result = await query('SELECT svalue FROM ini WHERE  fkey="apiClientPw"');
  // @ts-ignore
  const pwInDb:string = result[0].svalue;

  console.log('pw from user:');
  console.log(body.password);
  console.log('db pw');
  console.log(pwInDb);

  // comparing passwords when crypted with bcrypt.hashSync(req.body.password, 8)
  /* const passwordIsValid = bcrypt.compareSync(
    body.password,
    pwInDb
  ); */

  // checking if password was valid and send response accordingly
  if (body.password !== pwInDb) {
    res.writeHead(401, { 'Content-Type': 'application/json' });
    res.write(
      JSON.stringify({
        message: '\'Invalid Password!\'',
        accessToken: null
      })
    );
    return res;
  }
  const timeout = 86400;

  // signing token with user id
  const token = jwt.sign(
    { id: USERID },
    apiKey,
    { expiresIn: timeout }
  );

  // responding to client request with user profile success message and  access token .
  res.writeHead(200, { 'Content-Type': 'application/json' });
  res.write(
    JSON.stringify({
      message: 'Login successfull',
      accessToken: token
    })
  );
  return res;
}

const User = {};

export async function verifyToken (req, res) {
  console.log('verify...');
  console.log(req.headers);
  const auth = req.headers.authorization.split(' ');
  if (req.headers && req.headers.authorization && req.headers.authorization.split(' ')[0] === 'JWT') {
    const decode = await verify(req, auth[1]);
    console.log(decode);
    if (decode.id && decode.id == USERID) {
      console.log('client verified');
      return res;
    }
    else {
      res.writeHead(403, { 'Content-Type': 'application/json' });
      res.write(JSON.stringify({ message: 'Unauthorised access' }));
      res.end();
      return null;
    }
  }
  else {
    console.log('something went wrong...');
    res.end();
    return null;
  }
};
