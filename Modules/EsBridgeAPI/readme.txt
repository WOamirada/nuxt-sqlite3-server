## Bridge between ElasticSearch Api and Amirada System

- when calling a API function , set in the Header of the request
  authorization : JWT {{apiKey}}

- the apiKey can be found in the database with sql statement :
    SELECT svalue FROM ini WHERE  fkey="apiKey"

- there is only a password and no more users or user data. the password can be found with (sql statement):
   SELECT svalue FROM ini WHERE  fkey="apiClientPw"
