
export function parseReq (request) {
  return new Promise((resolve, _) => {
    const chunks = [];
    // 'data' event is emitted on every chunk received
    request.on('data', (chunk) => {
      // collecting the chunks in array
      chunks.push(chunk);
    });

    // when all chunks are received, 'end' event is emitted.
    request.on('end', () => {
      // joining all the chunks received
      const data = Buffer.concat(chunks);
      // data.toString() converts Buffer data to querystring format
      const querystring = data.toString();
      // URLSearchParams: takes querystring
      // & returns a URLSearchParams object instance.
      const parsedData = new URLSearchParams(querystring);
      let dataObj = {};
      // entries() method returns an iterator
      // allowing iteration through all key/value pairs
      const result = [];
      for (const pair of parsedData.entries()) {
        if (!pair[1]) {
          dataObj = JSON.parse(pair[0]);
        }
        else {
          console.log('more then one pair..');
        }
        result.push(dataObj);
      }
      // Now request data is accessible using dataObj
      resolve(result);
    });
  });
}
