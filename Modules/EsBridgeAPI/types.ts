export type TRequestTypes = 'GET' | 'POST' | 'PUT' | 'DELETE' ;

export enum eRequestTypes {
  get="GET",
  post="POST",
  put="PUT",
  delete="DELETE"
}
export interface ILoginBody {
  email:string,
  password:string
}

export interface IIncomingRequest {
  body: ILoginBody|object;
  method:TRequestTypes,
  on:(key:string, handler:(data:object)=>void )=>void,
}

export interface TResponseObject {
  write:(s:string)=>void
  end:()=>void
}
