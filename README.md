## Get Started

1. Install docker
2. for a test environment run runTestEnv.bat
3. start server with : npm run start


## Notes:
- if Docker not running, maybe theres an issue with WSL 2. You can fix it
  by installing this update under Step 4:
  https://docs.microsoft.com/de-de/windows/wsl/install-manual#step-4---download-the-linux-kernel-update-package

- had to edit start script in package.json, because for the serverMiddleware paths to for example to types.ts
  were not found. following fix was necessary:
  - install package "tsconfig-paths-webpack-plugin"
  - in package.json edit :
    { scripts:{ dev :  "node -r tsconfig-paths/register ./node_modules/nuxt/bin/nuxt.js" }}

## Database access via Prisma (thrown out maybe to be integrated again later)
- To sync your data model to your database schema, you’ll need to use prisma migrate CLI.
  npx prisma migrate dev --name init

  
