export default {
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: 'CustomerServer',
    htmlAttrs: {
      lang: 'en'
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },
  srcDir: "src/",
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/typescript
    '@nuxt/typescript-build',
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
  ],
  serverMiddleware: {
    '/sqlLite': '~/serverMiddleware/sqlLite',
    '/esAPI': '~/serverMiddleware/esAPI'
    // { path: '/esAPI', handler: '~/serverMiddleware/esAPI/index.ts' }
  },
  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
    extend(config, ctx) {
      config.node = {
        fs: 'empty'
      }
    }
  }
}
