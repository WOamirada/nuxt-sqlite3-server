!!!move to Modules/esBridgeAPI

===============================
=== initilizing MYSQL-DOCKER
===============================
1. access bash by typing:
    docker exec -u 0 -it mysql_container bash

2. access mysql shell by typing:
   mysql -u root -p

3.  set a password for mysql by typing:
  SET PASSWORD = PASSWORD('passwordhere');

4. make sure to save root-user and mysql password in project folder.

5. create a database by typing:
  CREATE DATABASE customerDB;

6. import database using vm-terminal by typing :
   mysql -p -u root customerDB < /usr/share/database/dbdump.sql   // TODO: explain docker mounting..
