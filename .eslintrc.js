module.exports = {
  root: true,
  extends: [
    '@nuxtjs/eslint-config-typescript',
  ],
  rules: {
    // Script
    "semi": ["error", "always"], // force semicolons add end of line
    "indent": ["error", 2, { "SwitchCase": 1 }], // standard indent
    // Templates
    "vue/no-v-html": ["off"],
    // line breaks, number of attributes etc.
    "vue/multiline-html-element-content-newline": ["error", {"allowEmptyLines": true}],
    "vue/singleline-html-element-content-newline": ["off"],
    "vue/max-attributes-per-line": ["error", {
      "singleline": 10,
      "multiline": {
        "max": 1,
        "allowFirstLine": false
      }
    }],
    "vue/no-parsing-error": ["error", {"invalid-first-character-of-tag-name": false}],
    // not yet defined/ open
    "camelcase": ["off"],
    "vue/attribute-hyphenation": ["off"],
    "dot-notation": ["off"], // problem with some resulsts from sql not really defined
    "linebreak-style":["off"],
    "no-lonely-if":["error"],
    "max-statements-per-line": 1,
    "brace-style":["error","stroustrup",{"allowSingleLine": true}],
  },
};
